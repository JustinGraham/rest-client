Rest-Client
===========

[![Build Status](https://drone.io/bitbucket.org/JustinGraham/rest-client/status.png)](https://drone.io/bitbucket.org/JustinGraham/rest-client/latest) [![Coverage Status](https://coveralls.io/repos/bitbucket/JustinGraham/rest-client/badge.svg?branch=master)](https://coveralls.io/bitbucket/JustinGraham/rest-client?branch=master)

The Rest-Client is an immutable fluent wrapper for javax.ws.rs.client.Client. 

#### Client example
```
public class ExampleClient {
    private static final Handler VALIDATE_200 = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            new ResponseValidator(response)
                .withStatus(200)
                .withMediaType(MediaType.APPLICATION_JSON_TYPE)
                .withHeaders(
                    new HashMap<String, List<String>> {{
                        put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(MediaType.APPLICATION_JSON));
                    }}
                )
                .verify();
            return response.readEntity(returnType);
        }
    }

    private final RestClient client;
    
    // Setup the base client so that it's domain is specific for this client.
    // The RestClient's endpoint will be {baseUrl}/v2/example for all executing methods
    public ExampleClient(@NotNull final String baseUrl) {
        this.client = new RestClient(baseUrl)
            .path("v2", "example")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .accept(MediaType.APPLICATION_JSON_TYPE)
            .status(Status.OK, VALIDATE_200)
            .status(Status.BAD_REQUEST, Handler.BAD_REQUEST)
            .status(Status.UNAUTHORIZED, Handler.UNAUTHORIZED);
    }
    
    // GET {baseUrl}/v2/example/{exampleId}
    public Example getExample(@NotNull final String exampleId) {
        return client.path(exampleId).get(Example.class);
    }
    
    // GET {baseUrl}/v2/example?query={exampleQuery}
    public Example getQueryExample(@NotNull final String exampleQuery) {
        return client.query("query", exampleQuery).get(Example.class);
    }
    
    // GET {baseUrl}/v2/example
    public Response getExampleResponse() {
        return client.get();
    }
    
    // POST {baseUrl}/v2/example
    public Example postExample(@NotNull final Example example) {
        return client.post(Entity.json(example), Example.class);
    }
}
```