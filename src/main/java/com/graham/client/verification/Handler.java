package com.graham.client.verification;

import com.graham.client.RestClient;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:Justin.af.graham@gmail.com">Justin Graham</a>
 * @since 5/27/2016
 */
public interface Handler {

    /**
     * The method used to validate a {@link Response} object. To use a custom {@link Handler} add all validation
     * logic into this method and provide it to your {@link RestClient} with the status it should
     * be ran against
     *
     * @param response the object to validate
     * @param returnType the class to deserialize into
     * @param <T> the type of class to be returned
     * @return the deserialized object
     * @throws Exception If the response could not be deserialized or the Handler throws an exception
     */
    <T> T verify(@NotNull final Response response, @NotNull final Class<T> returnType) throws Exception;

    /**
     * This is the expected handler. It doesn't do any validation of the response instead just deserializing the
     * response into the provided class
     */
    Handler EXPECTED = Response::readEntity;

    /**
     * This handler validates the response status code is 400 then throws a {@link BadRequestException} containing
     * the response body
     */
    Handler BAD_REQUEST = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
            throw new BadRequestException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 401 then throws a {@link NotAuthorizedException} containing
     * the response body
     */
    Handler UNAUTHORIZED = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.UNAUTHORIZED.getStatusCode());
            throw new NotAuthorizedException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 403 then throws a {@link ForbiddenException} containing
     * the response body
     */
    Handler FORBIDDEN = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.FORBIDDEN.getStatusCode());
            throw new ForbiddenException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 404 then throws a {@link NotFoundException} containing
     * the response body
     */
    Handler NOT_FOUND = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.NOT_FOUND.getStatusCode());
            throw new NotFoundException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 405 then throws a {@link NotAllowedException} containing
     * the response body
     */
    Handler METHOD_NOT_ALLOWED = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.METHOD_NOT_ALLOWED.getStatusCode());
            throw new NotAllowedException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 406 then throws a {@link NotAcceptableException} containing
     * the response body
     */
    Handler NOT_ACCEPTABLE = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.NOT_ACCEPTABLE.getStatusCode());
            throw new NotAcceptableException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 415 then throws a {@link NotSupportedException} containing
     * the response body
     */
    Handler UNSUPPORTED_MEDIA_TYPE = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode());
            throw new NotSupportedException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 500 then throws a {@link InternalServerErrorException}
     * containing the response body
     */
    Handler INTERNAL_SERVER_ERROR = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
            throw new InternalServerErrorException(response.readEntity(String.class), response);
        }
    };

    /**
     * This handler validates the response status code is 503 then throws a {@link ServiceUnavailableException}
     * containing the response body
     */
    Handler SERVICE_UNAVAILABLE = new Handler() {
        @Override
        public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
            assertThat(response.getStatus()).isEqualTo(Status.SERVICE_UNAVAILABLE.getStatusCode());
            throw new ServiceUnavailableException(response.readEntity(String.class), response);
        }
    };

    /**
     * This map will contain the default mapping of status codes to handlers. A user can override any default by
     * using the {@link RestClient#status(Integer, Handler)} or {@link RestClient#status(Status, Handler)} methods.
     */
    Map<Integer, Handler> HANDLER_MAP = new HashMap<Integer, Handler>() {{

        // 200 OK
        put(Status.OK.getStatusCode(), EXPECTED);

        // 201 Created
        put(Status.CREATED.getStatusCode(), EXPECTED);

        // 202 Accepted
        put(Status.ACCEPTED.getStatusCode(), EXPECTED);

        // 204 No Content
        put(Status.NO_CONTENT.getStatusCode(), EXPECTED);

        // 205 Reset Content
        put(Status.RESET_CONTENT.getStatusCode(), EXPECTED);

        // 206 Partial Content
        put(Status.PARTIAL_CONTENT.getStatusCode(), EXPECTED);

        // 400 Bad Request
        put(Status.BAD_REQUEST.getStatusCode(), BAD_REQUEST);

        // 401 Unauthorized
        put(Status.UNAUTHORIZED.getStatusCode(), UNAUTHORIZED);

        // 403 Forbidden
        put(Status.FORBIDDEN.getStatusCode(), FORBIDDEN);

        // 404 Not Found
        put(Status.NOT_FOUND.getStatusCode(), NOT_FOUND);

        // 405 Method Not Allowed
        put(Status.METHOD_NOT_ALLOWED.getStatusCode(), METHOD_NOT_ALLOWED);

        // 406 Not Acceptable
        put(Status.NOT_ACCEPTABLE.getStatusCode(), NOT_ACCEPTABLE);

        // 415 Unsupported Media Type
        put(Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode(), UNSUPPORTED_MEDIA_TYPE);

        // 500 Internal Server Error
        put(Status.INTERNAL_SERVER_ERROR.getStatusCode(), INTERNAL_SERVER_ERROR);

        // 503 Service Unavailable
        put(Status.SERVICE_UNAVAILABLE.getStatusCode(), SERVICE_UNAVAILABLE);
    }};
}

