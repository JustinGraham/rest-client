package com.graham.client.verification;

import org.assertj.core.api.SoftAssertions;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * This class uses builder methods to validating the {@link Response}. To validate with this you would want to create
 * a new {@link Handler} similar to the following:
 *
 * <pre>{@code
 *      Handler MY_CUSTOM_HANDLER = new Handler() {
 *          {@literal @}Override
 *          public <T> T verify(@NotNull Response response, @NotNull Class<T> returnType) throws Exception {
 *              new ResponseValidator(response)
 *                  .withStatus(200)
 *                  .withMediaType(MediaType.APPLICATION_JSON_TYPE)
 *                  .withHeaders(
 *                      new HashMap<String, List<String>> {{
 *                          put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(MediaType.APPLICATION_JSON));
 *                          put(HttpHeaders.AUTHORIZATION, Collections.singletonList(MY_AUTHZ_TOKEN));
 *                      }}
 *                  )
 *                  .verify();
 *
 *                  return response.readEntity(returnType);
 *          }
 *      }
 * }</pre>
 *
 * @author <a href="mailto:Justin.af.graham@gmail.com">Justin Graham</a>
 * @since 5/31/2016
 *
 * @see MediaType
 * @see HttpHeaders
 */
public class ResponseValidator {

    private final SoftAssertions softly = new SoftAssertions();
    private final Integer actualStatus;
    private final MediaType actualMediaType;
    private final MultivaluedMap<String, String> actualHeaders;

    public ResponseValidator(@NotNull final Response response) {
        this.actualStatus = response.getStatus();
        this.actualMediaType = response.getMediaType();
        this.actualHeaders = response.getStringHeaders();
    }

    public ResponseValidator withStatus(@NotNull final Integer expectedStatus) {
        softly.assertThat(actualStatus)
                .withFailMessage("Actual status code [%s] does not match expected [%s]", actualStatus, expectedStatus)
                .isEqualTo(expectedStatus);
        return this;
    }

    public ResponseValidator withMediaType(@NotNull final MediaType expectedMediaType) {
        softly.assertThat(actualMediaType.toString())
                .withFailMessage("Actual media type [%s] does not match expected [%s]",
                        actualMediaType.toString(), expectedMediaType.toString())
                .isEqualTo(expectedMediaType.toString());
        return this;
    }

    public ResponseValidator withHeaders(@NotNull final Map<String, List<String>> expectedHeaders) {
        if (!expectedHeaders.isEmpty()) {
            softly.assertThat(actualHeaders)
                    .withFailMessage("Actual headers %s does not contain expected %s",
                            actualHeaders.entrySet(), expectedHeaders.entrySet())
                    .containsAllEntriesOf(expectedHeaders);
        }
        return this;
    }

    public void verify() {
        softly.assertAll();
    }
}
