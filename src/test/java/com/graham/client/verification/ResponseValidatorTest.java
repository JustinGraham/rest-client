package com.graham.client.verification;

import mockit.Expectations;
import mockit.Injectable;
import org.assertj.core.api.SoftAssertionError;
import org.testng.annotations.Test;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.ALLOW;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE;

/**
 * @author <a href="mailto:Justin.af.graham@gmail.com">Justin Graham</a>
 * @since 6/1/2016
 */
@Test(groups = "unit")
public class ResponseValidatorTest {

    @Injectable private Response response;

    private final MultivaluedMap<String, String> actualHeaders = new MultivaluedHashMap<String, String>() {{
        put(ACCEPT, singletonList(APPLICATION_JSON));
        put(ALLOW, singletonList(APPLICATION_XML));
        put(CONTENT_TYPE, singletonList(APPLICATION_FORM_URLENCODED));
    }};

    @Test
    public void testWithStatus() throws Exception {
        new Expectations() {{ response.getStatus(); result = 200; }};
        new ResponseValidator(response).withStatus(200).verify();
    }

    @Test(expectedExceptions = SoftAssertionError.class)
    public void testWithStatusError() throws Exception {
        new Expectations() {{ response.getStatus(); result = 200; }};
        new ResponseValidator(response).withStatus(400).verify();
    }

    @Test
    public void testWithMediaType() throws Exception {
        new Expectations() {{ response.getMediaType(); result = APPLICATION_JSON_TYPE; }};
        new ResponseValidator(response).withMediaType(APPLICATION_JSON_TYPE).verify();
    }

    @Test(expectedExceptions = SoftAssertionError.class)
    public void testWithMediaTypeError() throws Exception {
        new Expectations() {{ response.getMediaType(); result = APPLICATION_JSON_TYPE; }};
        new ResponseValidator(response).withMediaType(APPLICATION_XML_TYPE).verify();
    }

    @Test
    public void testWithHeaders() throws Exception {
        new Expectations() {{ response.getStringHeaders(); result = actualHeaders; }};
        final Map<String, List<String>> expected = new HashMap<String, List<String>>() {{
            put(ACCEPT, singletonList(APPLICATION_JSON));
        }};
        new ResponseValidator(response).withHeaders(expected).verify();
    }

    @Test
    public void testWithHeadersEmpty() throws Exception {
        new Expectations() {{ response.getStringHeaders(); result = actualHeaders; }};
        new ResponseValidator(response).withHeaders(Collections.emptyMap()).verify();
    }

    @Test(expectedExceptions = SoftAssertionError.class)
    public void testWithHeadersError() throws Exception {
        new Expectations() {{ response.getStringHeaders(); result = actualHeaders; }};
        final Map<String, List<String>> expected = new HashMap<String, List<String>>() {{
            put(ALLOW, singletonList(APPLICATION_JSON));
        }};
        new ResponseValidator(response).withHeaders(expected).verify();
    }
}
