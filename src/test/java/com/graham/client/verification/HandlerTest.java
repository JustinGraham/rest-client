package com.graham.client.verification;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Verifications;
import org.testng.annotations.Test;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.core.Response;

/**
 * @author <a href="mailto:Justin.af.graham@gmail.com">Justin Graham</a>
 * @since 6/1/2016
 */
@Test(groups = "unit")
public class HandlerTest {
    private static final String EXPECTED_MSG = ".*param.*";

    @Injectable Response response;

    private final String param = "param";

    @Test
    public void testExpectedHandler() throws Exception {
        Handler.EXPECTED.verify(response, String.class);
        new Verifications() {{ response.readEntity(String.class); }};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testBadRequestHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.BAD_REQUEST.verify(response, String.class);
    }

    @Test(expectedExceptions = BadRequestException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testBadRequestHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 400;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.BAD_REQUEST.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testUnauthorizedHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.UNAUTHORIZED.verify(response, String.class);
    }

    @Test(expectedExceptions = NotAuthorizedException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testUnauthorizedHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 401;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.UNAUTHORIZED.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testForbiddenHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.FORBIDDEN.verify(response, String.class);
    }

    @Test(expectedExceptions = ForbiddenException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testForbiddenHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 403;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.FORBIDDEN.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testNotFoundHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.NOT_FOUND.verify(response, String.class);
    }

    @Test(expectedExceptions = NotFoundException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testNotFoundHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 404;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.NOT_FOUND.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testNotAllowedHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.METHOD_NOT_ALLOWED.verify(response, String.class);
    }

    @Test(expectedExceptions = NotAllowedException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testNotAllowedHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 405;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.METHOD_NOT_ALLOWED.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testNotAcceptableHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.NOT_ACCEPTABLE.verify(response, String.class);
    }

    @Test(expectedExceptions = NotAcceptableException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testNotAcceptableHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 406;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.NOT_ACCEPTABLE.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testUnsupportedHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.UNSUPPORTED_MEDIA_TYPE.verify(response, String.class);
    }

    @Test(expectedExceptions = NotSupportedException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testUnsupportedHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 415;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.CLIENT_ERROR;
        }};
        Handler.UNSUPPORTED_MEDIA_TYPE.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testISEHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.INTERNAL_SERVER_ERROR.verify(response, String.class);
    }

    @Test(expectedExceptions = InternalServerErrorException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testISEHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 500;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.SERVER_ERROR;
        }};
        Handler.INTERNAL_SERVER_ERROR.verify(response, String.class);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testServiceUnavailableHandlerInvalidStatus() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 200;
        }};
        Handler.SERVICE_UNAVAILABLE.verify(response, String.class);
    }

    @Test(expectedExceptions = ServiceUnavailableException.class, expectedExceptionsMessageRegExp = EXPECTED_MSG)
    public void testServiceUnavailableHandler() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 503;
            response.readEntity(String.class); result = param;
            response.getStatusInfo().getFamily(); result = Response.Status.Family.SERVER_ERROR;
        }};
        Handler.SERVICE_UNAVAILABLE.verify(response, String.class);
    }
}
