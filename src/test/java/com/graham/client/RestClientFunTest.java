package com.graham.client;

import mockit.Expectations;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import static javax.ws.rs.client.Entity.text;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.Family.CLIENT_ERROR;
import static javax.ws.rs.core.Response.Status.Family.SERVER_ERROR;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author <a href="mailto:Justin.af.graham@gmail.com">Justin Graham</a>
 * @since 6/8/2016
 */
@Test(groups = "functional")
public class RestClientFunTest {

    private static final String JSON_RESPONSE = "{\"key\":\"value\",\"key2\":5}";
    private static final String RESPONSE_REGEX = "\\{\"key\":\"value\",\"key2\":5}";
    private static final String BASE_URI = "http://www.rest.client.test.com";
    private static final String PARAM = "param";

    @Mocked ClientBuilder clientBuilder;
    @Mocked Client client;
    @Mocked WebTarget webTarget;
    @Mocked Invocation.Builder builder;
    @Mocked Response response;
    @Mocked Cookie cookie;

    @Tested RestClient restClient;

    @BeforeMethod
    public void setUp() throws Exception {
        // Mock out calls to external libraries
        new NonStrictExpectations() {{
            ClientBuilder.newClient(); result = client;
            client.target((UriBuilder) any); result = webTarget;
            webTarget.request((MediaType[]) any).accept((MediaType[]) any); result = builder;
            builder.get(); result = response;
            builder.put((Entity) any); result = response;
            builder.post((Entity) any); result = response;
            builder.delete(); result = response;
            response.readEntity((Class<Object>) any); result = JSON_RESPONSE;
        }};

        // Setup a crazy big rest client to validate the different handlers
        restClient = new RestClient(BASE_URI)
                .path(PARAM, PARAM)
                .query(PARAM, PARAM)
                .header(PARAM, PARAM)
                .cookie(PARAM, PARAM)
                .cookie(cookie)
                .request(APPLICATION_JSON_TYPE)
                .accept(APPLICATION_JSON_TYPE);
    }

    @Test(
            expectedExceptions = IllegalStateException.class,
            expectedExceptionsMessageRegExp = ".*Response status code \\[1000] has not be added to the RestClient " +
                    "as a valid or invalid status. Expected one of .*",
            description = "Validates the RestClient requires unknown HTTP response codes to be added using the " +
                    "status() method"
    )
    public void testGetNoStatusHandler() throws Exception {
        new Expectations() {{ response.getStatus(); result = 1000; }};
        new RestClient(BASE_URI).get(String.class);
    }

    @Test(description = "Validates the EXPECTED handler does not throw an exception")
    public void testGet200() throws Exception {
        new Expectations() {{ response.getStatus(); result = 200; }};
        assertThat(restClient.get(String.class)).isEqualTo(JSON_RESPONSE);
    }

    @Test(
            expectedExceptions = BadRequestException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the BAD_REQUEST handler throws a BadRequestException with the response body"
    )
    public void testPut400() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 400;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.put(text(JSON_RESPONSE), String.class);
    }

    @Test(
            expectedExceptions = NotAuthorizedException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the UNAUTHORIZED handler throws a NotAuthorizedException with the response body"
    )
    public void testPost401() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 401;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.post(text(JSON_RESPONSE), String.class);
    }

    @Test(
            expectedExceptions = ForbiddenException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the FORBIDDEN handler throws a ForbiddenException with the response body"
    )
    public void testDelete403() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 403;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = NotFoundException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the NOT_FOUND handler throws a NotFoundException with the response body"
    )
    public void testDelete404() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 404;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = NotAllowedException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the METHOD_NOT_ALLOWED handler throws a NotAllowedException with the response body"
    )
    public void testDelete405() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 405;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = NotAcceptableException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the NOT_ACCEPTABLE handler throws a NotAcceptableException with the response body"
    )
    public void testDelete406() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 406;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = NotSupportedException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the UNSUPPORTED_MEDIA_TYPE handler throws a NotSupportedException " +
                    "with the response body"
    )
    public void testDelete415() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 415;
            response.getStatusInfo().getFamily(); result = CLIENT_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = InternalServerErrorException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the INTERNAL_SERVER_ERROR handler throws a InternalServerErrorException " +
                    "with the response body"
    )
    public void testDelete500() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 500;
            response.getStatusInfo().getFamily(); result = SERVER_ERROR;
        }};
        restClient.delete(String.class);
    }

    @Test(
            expectedExceptions = ServiceUnavailableException.class,
            expectedExceptionsMessageRegExp = RESPONSE_REGEX,
            description = "Validates the SERVICE_UNAVAILABLE handler throws a ServiceUnavailableException " +
                    "with the response body"
    )
    public void testDelete503() throws Exception {
        new Expectations() {{
            response.getStatus(); result = 503;
            response.getStatusInfo().getFamily(); result = SERVER_ERROR;
        }};
        restClient.delete(String.class);
    }
}
